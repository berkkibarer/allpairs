# README #

### Credits ###
Full credit goes to James Bach http://www.satisfice.com/tools.shtml

### What is this repository for? ###

All pairs or pairwise method is used to narrow down the test cases and helps testers determine the priority.

### How do I get set up? ###

Perl installation required

### How to use ###

Allpairs prepares test cases to cover all pairings of a set of variables.

EXE usage:    allpairs datafile
Script usage: perl allpairs.pl datafile

"datafile" is a tab delimited text file that consists of labelled columns
of variables. The first row of the table is treated as labels.
If you copy from Excel into a text file it will be the right format.

Example: allpairs test.txt

...where test.txt contains:

colors	cars	times
purple	Mazda	night
blue	Ford	day
silver		dawn

...will produce this output:


TEST CASES
case	colors	cars	times	pairings
1	purple	Mazda	night	3
2	purple	Ford	day	3
3	blue	Ford	night	3
4	blue	Mazda	day	3
5	silver	Mazda	dawn	3
6	silver	Ford	night	2
7	purple	Ford	dawn	2
8	blue	~Mazda	dawn	1
9	silver	~Mazda	day	1

PAIRING DETAILS
var1	var2	value1	value2	appearances	cases
colors	times	purple	night	1	1
colors	times	purple	day	1	2
colors	times	purple	dawn	1	7
colors	times	blue	night	1	3
colors	times	blue	day	1	4
colors	times	blue	dawn	1	8
colors	times	silver	night	1	6
colors	times	silver	day	1	9
colors	times	silver	dawn	1	5
colors	cars	purple	Mazda	1	1
colors	cars	purple	Ford	2	2, 7
colors	cars	blue	Mazda	2	4, 8
colors	cars	blue	Ford	1	3
colors	cars	silver	Mazda	2	5, 9
colors	cars	silver	Ford	1	6
times	cars	night	Mazda	1	1
times	cars	night	Ford	2	3, 6
times	cars	day	Mazda	2	4, 9
times	cars	day	Ford	1	2
times	cars	dawn	Mazda	2	5, 8
times	cars	dawn	Ford	1	7

### Contact ###

Berk Kibarer
berkkibarer@gmail.com